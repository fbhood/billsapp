<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paidouts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('account_id')->default(1);
            $table->text('date')->date();
            $table->text('type')->nullable();
            $table->string('description');
            $table->float('value');
            $table->float('balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paidouts');
    }
}
