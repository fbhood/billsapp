@extends('layouts.app')

@section('content')
<div class="content">
           
               
           <div class="container-fluid">
               <div class="row">

                   <div class="col-9">

                       <div class="row p-3">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs py-3" id="myTab" role="tablist">
                           <li class="nav-item">
                               <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Account Overview</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="paidin-tab" data-toggle="tab" href="#paidin" role="tab" aria-controls="paidin" aria-selected="false">Income</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="paidouts-tab" data-toggle="tab" href="#paidouts" role="tab" aria-controls="paidouts" aria-selected="false">Expenses</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
                           </li>
                           </ul>

                           <!-- Tab panes -->
                           <div class="tab-content container py-5">
                               <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                                  <h2> General Account Overview</h2>

                                   <div class="row">
                                       <div class="col-xs-12 col-lg-8">
                                           <overview-graph-component class="pb-5" width="300" :keys="{{ $overview->keys() }}" :values="{{ $overview->values()}}" ></overview-graph-component>
                                           <div class="alert alert-success" role="alert">
                                               <strong>Total Income: {{ $total_income}}</strong>
                                           </div>
                                           <div class="alert alert-danger" role="alert">
                                               <strong>Total Expenses: {{ $total_expenses}}</strong>
                                           </div>
                                           <div class="alert alert-info" role="alert">
                                               <strong>Account Balance: {{ $balance }}</strong>
                                           </div>
                                       </div>
                                       <div class="col-xs-12 col-lg-4">
                                       
                                           <table class="table table-striped table-inverse table-responsive">
                                               <thead>
                                                   <tr>
                                                       <th class="w-30">Category</th>
                                                       <th class="w-70">Value</th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                               @foreach($overview as $category => $value)
                                                   <tr>
                                                       <td scope="row">{{ $category }}</td>
                                                       <td>{{ $value}}</td>
                                                   </tr>
                                               @endforeach
                                               <tr>
                                                       <th>Balance </th>
                                                       <th>£ {{ $balance }} </th>
                                                   </tr>
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>

                                   <hr>
                               </div>
                               <div class="tab-pane" id="paidin" role="tabpanel" aria-labelledby="paidin-tab">
                                      
                                      <div class="container">
                                          <div class="row">
                                              <div class="col-5">
                                              <income-graph-component class="py-3" :keys="{{ $paidins_results->keys() }}" :values="{{ $paidins_results->values()}}" ></income-graph-component>

                                              </div>
                                              <div class="col">
                                              <h2> Total Income £ {{ $total_income }}</h2>
                                               <ul class="list-unstyled"> 
                                               @foreach($paidins_results as $key => $val) 
                                                   <li> <span>{{ $key }}: £</span> {{ $val }}</li>
                                               @endforeach 
                                               </ul>
                                              </div>
                                              
                                          </div>
                                      </div> 


                                   <div id="paidin" class="col-12" >
                                       <form id="income_search" action="/search" method="get">
                                           @csrf

                                           <div class="input-group">
                                             <label for="description"></label>
                                             <input type="text" class="form-control" name="description" id="description" aria-describedby="helpDescriptionId" placeholder="ie. rent">
                                               <div class="input-group-append">
                                                   <button form="income_search" type="submit" class="btn btn-primary">Submit</button>
                                               </div><br>

                                           </div>
                                           <small id="helpDescriptionId" class="form-text text-muted">Search items by description</small>

                                       </form>

                                       <section id="paidin_table" class="py-3">
                                       
                                           @if($paidins)
                                               <table class="table table-striped table-inverse table-responsive  ">
                                                   <thead>
                                                       <tr>
                                                           <th>Date</th>
                                                           <th>Type</th>
                                                           <th>Description</th>
                                                           <th>Value</th>
                                                       </tr>
                                                   </thead>
                                                   <tbody>
                                                   @foreach($paidins as $paidin)

                                                       <tr>
                                                           <td scope="row">{{$paidin->date}}</td>
                                                           <td>{{$paidin->type}}</td>
                                                           <td>{{$paidin->description}}</td>
                                                           <td>{{$paidin->value}}</td>
                                                       </tr>
                                                   @endforeach

                                                   </tbody>
                                               </table>
                                               
                                               
                                           @endif
                                       </section>

                                   </div>                                
                               </div>
                               <div class="tab-pane" id="paidouts" role="tabpanel" aria-labelledby="paidouts-tab">
                                      
                                      <div class="container">
                                          <div class="row">
                                              <div class="col-5">
                                              <graph-component class="py-3" :keys="{{ $paidouts_results->keys() }}" :values="{{ $paidouts_results->values()}}" ></graph-component>

                                              </div>
                                              <div class="col">
                                              <h2> Total Expenses £ {{ $total_expenses }}</h2>
                                              <ul class="list-unstyled"> 
                                               @foreach($paidouts_results as $key => $val) 
                                                   <li> <span>{{ $key }}: £</span> {{ $val }}</li>
                                               @endforeach 
                                               </ul>
                                              </div>
                                              
                                          </div>
                                      </div>
                                   

                                   <div id="paidout" class="col-12" > 
                                       <form id="search_expenses" action="/search" method="get">
                                           @csrf

                                           <div class="input-group">
                                             <label for="description"></label>
                                             <input type="text" class="form-control" name="description" id="description" aria-describedby="helpDescriptionId" placeholder="ie. rent">
                                               <div class="input-group-append">
                                                   <button form="search_expenses" type="submit" class="btn btn-primary">Submit</button>
                                               </div><br>

                                           </div>
                                           <small id="helpDescriptionId" class="form-text text-muted">Search items by description</small>

                                       </form>


                                       <section id="paidout_table" class="py-3">

                                           @if($paidouts)

                                               <table class="table table-striped table-inverse table-responsive  ">
                                                   <thead>
                                                       <tr>
                                                           <th>Date</th>
                                                           <th>Type</th>
                                                           <th>Description</th>
                                                           <th>Value</th>
                                                       </tr>
                                                   </thead>
                                                   <tbody>
                                                   @foreach($paidouts as $paidout)

                                                       <tr>
                                                           <td scope="row">{{$paidout->date}}</td>
                                                           <td>{{$paidout->type}}</td>
                                                           <td>{{$paidout->description}}</td>
                                                           <td>{{$paidout->value}}</td>
                                                       </tr>
                                                   @endforeach

                                                   </tbody>
                                               </table>
                                           
                                           @endif
                                       </section>
                                   </div>
                               </div>
                               <div class="tab-pane" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                                  
                                   <div id="bankaccounts_accordion" role="tablist" aria-multiselectable="true" class="py-5">
                                       <div class="card">
                                           <div class="card-header" role="tab" id="section1HeaderId">
                                               <h5 class="mb-0">
                                                   <a data-toggle="collapse" data-parent="#bankaccounts_accordion" href="#section1ContentId" aria-expanded="true" aria-controls="section1ContentId">
                                               Add a new bank account
                                           </a>
                                               </h5>
                                           </div>
                                           <div id="section1ContentId" class="collapse in" role="tabpanel" aria-labelledby="section1HeaderId">
                                               <div class="card-body">
                                                   
                                               <div class="card p-5">    
                                                   <form id="new_bank_account" action="/bankaccounts" method="post">
                                                       @csrf 

                                                       <div class="form-group">
                                                       <label for="bank_name">Bank Name</label>
                                                       <input type="text" name="bank_name" id="bank_name" class="form-control w-100" placeholder="Your bank name here: ie. Natwest" aria-describedby="bankNameHelper">
                                                       <small id="bankNameHelper" class="text-muted">Add the name of your bank here</small>
                                                       </div>
                                                       <div class="form-group">
                                                       <label for="account_name">Account Name</label>
                                                       <input type="text" name="account_name" id="account_name" class="form-control" placeholder="Your name here: ie. Mario Rossi" aria-describedby="accountNameHelper">
                                                       <small id="accountNameHelper" class="text-muted">Your name here</small>
                                                       </div>
                                                       <div class="form-group">
                                                       <label for="account_number">Account Number</label>
                                                       <input type="text" name="account_number" id="account_number" class="form-control" placeholder="Your account number: ie. 6555665552 " aria-describedby="accountNumberHelper">
                                                       <small id="accountNumberHelper" class="text-muted">Add your account number here.</small>
                                                       </div>  
                                                       <div class="form-group">
                                                       <button type="submit" class="btn btn-primary">Add account</button>
                                                       </div>
                                                   </form>
                                               </div>

                                               </div>
                                           </div>
                                       </div>
                                       <div class="card">
                                           <div class="card-header" role="tab" id="section2HeaderId">
                                               <h5 class="mb-0">
                                                   <a data-toggle="collapse" data-parent="#bankaccounts_accordion" href="#section2ContentId" aria-expanded="true" aria-controls="section2ContentId">
                                               Update accounts
                                           </a>
                                               </h5>
                                           </div>
                                           <div id="section2ContentId" class="collapse in" role="tabpanel" aria-labelledby="section2HeaderId">
                                               
                                               @foreach($bankaccount as $account)
                                               <div class="card-body">
                                                   

                                                   <div class="card p-5">    
                                                       <form id="update_form_{{ $account->id}}" action="/bankaccounts/{{ $account->id }}" method="post">
                                                           @csrf
                                                           @method('PUT') 

                                                           <div class="form-group">
                                                           <label for="bank_name">Bank Name</label>
                                                           <input type="text" name="bank_name" id="bank_name" class="form-control w-100" placeholder="Your bank name here: ie. Natwest" value="{{ $account->bank_name }}" aria-describedby="bankNameHelper">
                                                           <small id="bankNameHelper" class="text-muted">Add the name of your bank here</small>
                                                           </div>
                                                           <div class="form-group">
                                                           <label for="account_name">Account Name</label>
                                                           <input type="text" name="account_name" id="account_name" class="form-control" placeholder="Your name here: ie. Mario Rossi" value="{{ $account->account_name }}"  aria-describedby="accountNameHelper">
                                                           <small id="accountNameHelper" class="text-muted">Your name here</small>
                                                           </div>
                                                           <div class="form-group">
                                                           <label for="account_number">Account Number</label>
                                                           <input type="text" name="account_number" id="account_number" class="form-control" placeholder="Your account number: ie. 6555665552 " value="{{ $account->account_number }}" aria-describedby="accountNumberHelper">
                                                           <small id="accountNumberHelper" class="text-muted">Add your account number here.</small>
                                                           </div>  
                                                           <div class="form-group">
                                                           <button form="update_form_{{ $account->id }}" type="submit" class="btn btn-primary">Update account</button>
                                                           </div>
                                                       </form>
                                                   </div>

                                               </div>
                                               @endforeach


                                           </div>
                                       </div>
                                   </div>


                                   @if($bankaccount)
                                      @foreach($bankaccount as $account)
                                           <div class="col">
                                               
                                          
                                           <div class="card bg-dark text-white">
                                               <div class="card-body">
                                                   <h4 class="card-title">{{ $account->bank_name}}</h4>
                                                   <p class="card-text">{{ $account->account_name}}</p>
                                                   <p class="card-text">{{ $account->account_number}}</p>
                                                   
                                                   <p class="card-text"> Balance: {{ $balance }}</p>
                                                   
                                                   @if($balance <= 0 ) 
                                                       <div class="alert alert-danger" role="alert">
                                                           <strong>Danger! 0 or less money! no party</strong>
                                                           <p>Lots of debts! balance under £ {{$balance}}</p>
                                                       </div>
                                                   @elseif($balance >= 250 )
                                                       <div class="alert alert-warning" role="alert">
                                                         <h4 class="alert-heading">Alert! just above the void</h4>
                                                         <p>Low balance! only £ {{ $balance }} left on this account</p>
                                                       </div>
                                                   @elseif($balance >= 800)
                                                   <div class="alert alert-info" role="alert">
                                                     <h4 class="alert-heading">Still can't pay the bills!</h4>
                                                     <p> Account Balance: £ {{ $balance }} </p>
                                                     <p class="mb-0"></p>
                                                   </div>
                                                   @else
                                                   <div class="alert alert-success " role="alert">
                                                       <strong> OK! Balance £ {{ $balance }} </strong>
                                                   </div>
                                                   @endif
                                               </div>
                                           </div>
                                       </div>
                                           
                                      @endforeach                     
                                   @endif

                               </div>
                           </div>



                       </div>

                   </div> 

                   <div id="sidebar" class="col-3">
                       <div class="card p-3">
                           <h3 class="card-title">Sidebar</h3>
                           <div class="alert alert-danger" role="alert">
                               <strong>Warning!</strong> Do not upload the same file twice to avoid duplicated entries. 
                           </div>
                           <form enctype="multipart/form-data" id="paidin_form" action="/import-paidin" method="post">
                               @csrf
                               <div class="form-group">
                           
                                   <label for="paidin">Import PaidIn </label>
                                   <input type="file" class="form-control-file" id="paidin" name="paidin" placeholder="paid_in_file" aria-describedby="paidInHelper">
                                   <small id="paidInHelper" class="form-text text-muted">Import your paid in file</small>
                               </div>
                               <div class="form-group-append">
                                   <button form="paidin_form" type="submit" class="btn btn-primary">Import</button>
                               </div>
                           </form>
                           <hr class="py-3">
                           <form enctype="multipart/form-data" id="paidout_form" action="/import-paidout" method="post" >
                               @csrf 

                               <div class="form-group">
                                   <label for="paidouts">Import Paidouts</label>
                                   <input type="file" class="form-control-file" name="paidouts" id="paidouts" placeholder="paid_outs_file" aria-describedby="paidOutsHelper">
                                   <small id="paidOutsHelper" class="form-text text-muted">Import your paid outs file</small>
                               </div>
                               <div class="form-group-append">
                                   <button form="paidout_form" type="submit" class="btn btn-primary">Import</button>
                               </div>
                           </form>
                       </div>
                   </div>         
                
                </div>
              
           </div>
       
       
       </div>
@endsection
