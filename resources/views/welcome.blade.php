@extends('layouts.app')

@section('content')
            <div class="content">
           
               
                <div class="container-flex">

                <div class="jumbotron-fluid text-center bg-dark text-white p-2">
                    <h1 class="display-3 pt-3">{{ config( 'app.name') }}</h1>
                    <p class="lead">Record your income and expenses easly. Dispaly results in a nicely formatted graphics. Import transactions from your bank account. </p>
                    <hr class="my-2">
                    <p> Login to use start using the app </p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="/login" role="button">Login</a>
                    </p>
                </div>

                    <div class="container p-5">
                       
                       <p class="lead py-3">
                            The {{ config('app.name')}} is a simple way to import your bank trasactions and keep track of your expenses and income.
                            The beta version works with one user and one account only.
                            <ol>
                                <li> Import from a .csv file all your expenses from your bank account </li>
                                <li> Import from a .csv file all your earnings from your bank account </li>
                                <li> Display records in Graphics and styled tables </li>
                                <li> Search in and out transactions </li>
                                <li> Add/Edit Bank accounts </li>
                                <li> Display Payments and Earnings by Category</li>
                                <li> Display Account Balance and alerts</li>
                                
                            </ol>    
                       </p>

                    </div>
                </div>         
            
            
            </div>
@endsection