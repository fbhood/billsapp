@extends('layouts.app')

@section('content')
            <div class="content">
           
               
                <div class="container">
                    <div class="row">
    
                        <div class="col-9">

                            <div class="row">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                
                                <li class="nav-item active">
                                    <a class="nav-link" id="results-tab" data-toggle="tab" href="#results" role="tab" aria-controls="results" aria-selected="false">Search Results</a>
                                </li>
                                
                               
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content container py-5">
                               
                                    <div class="tab-pane" id="results" role="tabpanel" aria-labelledby="results-tab">
                                           



                                        <div id="results" class="col-12" >
                                            <form action="/search" method="get">
                                                <div class="input-group">
                                                  <label for="description"></label>
                                                  <input type="text" class="form-control" name="description" id="description" aria-describedby="helpDescriptionId" placeholder="ie. rent">
                                                    <div class="input-group-append">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div><br>

                                                </div>
                                                <small id="helpDescriptionId" class="form-text text-muted">Search items by description</small>

                                            </form>

                                            <section id="results_table" class="py-3">
                                            
                                                @if($search_results)
                                                    <table class="table table-striped table-inverse table-responsive  ">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Type</th>
                                                                <th>Description</th>
                                                                <th>Value</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($search_results as $results)

                                                            <tr>
                                                                <td scope="row">{{$results->date}}</td>
                                                                <td>{{$results->type}}</td>
                                                                <td>{{$results->description}}</td>
                                                                <td>{{$results->value}}</td>
                                                            </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                    
                                                    
                                                @endif
                                            </section>

                                        </div>                                
                                    </div>
                                    
                                   
                                </div>



                            </div>

                        </div> 

                        <div id="sidebar" class="col-3">
                            <div class="card p-2">
                                <h3 class="card-title">Sidebar</h3>
                                <div class="alert alert-danger" role="alert">
                                    <strong>Warning!</strong> Do not upload the same file twice to avoid duplicated entries. 
                                </div>
                                <form enctype="multipart/form-data" id="paidin_form" action="/import-paidin" method="post">
                                    @csrf
                                    <div class="form-group">
                                
                                        <label for="paidin">Import PaidIn </label>
                                        <input type="file" class="form-control-file" id="paidin" name="paidin" placeholder="paid_in_file" aria-describedby="paidInHelper">
                                        <small id="paidInHelper" class="form-text text-muted">Import your paid in file</small>
                                    </div>
                                    <div class="form-group-append">
                                        <button form="paidin_form" type="submit" class="btn btn-primary">Import</button>
                                    </div>
                                </form>
                                <hr class="py-3">
                                <form enctype="multipart/form-data" id="paidout_form" action="/import-paidout" method="post" >
                                    @csrf 

                                    <div class="form-group">
                                        <label for="paidouts">Import Paidouts</label>
                                        <input type="file" class="form-control-file" name="paidouts" id="paidouts" placeholder="paid_outs_file" aria-describedby="paidOutsHelper">
                                        <small id="paidOutsHelper" class="form-text text-muted">Import your paid outs file</small>
                                    </div>
                                    <div class="form-group-append">
                                        <button form="paidout_form" type="submit" class="btn btn-primary">Import</button>
                                    </div>
                                </form>
                            </div>
                        </div>         
                     
                     </div>
                   
                </div>
            
            
            </div>
@endsection