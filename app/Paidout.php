<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paidout extends Model
{
    //
    protected $guarded = [];

    public function bankaccount(){
        $this->belongsTo(App\BankAccount::class);
    }

}
