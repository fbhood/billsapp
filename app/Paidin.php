<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Paidin extends Model
{
    //
    protected $guarded = [];

    public function bankaccount(){
       return $this->belongsTo(App\BankAccount::class);
    }
}
