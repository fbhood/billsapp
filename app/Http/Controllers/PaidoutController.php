<?php

namespace App\Http\Controllers;

use App\Paidout;
use Illuminate\Http\Request;
use App\Imports\ImportPaidouts;
use Maatwebsite\Excel\Facades\Excel;

class PaidoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request, Paidout $paidout)
    {
        $file = $request->paidouts;
        Excel::import(new ImportPaidouts, $file);
        return redirect('/')->with('success', 'All good!');


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate request

        // create new paidout


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paidout  $paidout
     * @return \Illuminate\Http\Response
     */
    public function show(Paidout $paidout)
    {
        // Show single paidout
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paidout  $paidout
     * @return \Illuminate\Http\Response
     */
    public function edit(Paidout $paidout)
    {
        // show form to edit paidout
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paidout  $paidout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paidout $paidout)
    {
        // update the requested paidout 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paidout  $paidout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paidout $paidout)
    {
        // delete the paidout
        $paidout->delete();
        return back();
    }
}
