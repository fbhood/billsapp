<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paidin;
use App\Paidout;
use App\Bankaccount;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $paidouts = Paidout::all();
        $paidins = Paidin::all();
        $auth_user_id = auth()->id();
        $bankaccount = Bankaccount::where('user_id', $auth_user_id)->get();
        $paidouts_results = DB::table('paidouts')->where('account_id', 1)->selectRaw("type, SUM(value) as paid")->groupBy('type')->pluck('paid','type');
        $paidins_results = DB::table('paidins')->where('account_id', 1)->selectRaw("type, SUM(value) as earned")->groupBy('type')->pluck('earned','type');
        
        $sum_paid_by_type = DB::table('paidouts')->where('account_id', 1)->selectRaw("type, SUM(value) as sum")->groupBy('type');
        $sum_earnings_by_type = DB::table('paidins')->where('account_id', 1)->selectRaw("type, SUM(value) as sum")->groupBy('type');
        
        $overview = $sum_earnings_by_type->union($sum_paid_by_type)->pluck('sum', 'type'); 
        $total_expenses = $paidouts->sum('value');
        $total_income   = $paidins->sum('value');
        $balance = $total_income + $total_expenses;
        //dd($balance);
        return view('home', compact('paidouts', 'bankaccount', 'paidins', 'paidouts_results','paidins_results','overview', 'balance', 'total_income','total_expenses'));
    }
}
