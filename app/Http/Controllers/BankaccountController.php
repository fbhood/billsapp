<?php

namespace App\Http\Controllers;

use App\Bankaccount;
use Illuminate\Http\Request;
class BankaccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BankAccount $bankaccount)
    {
        //
        //dd($request->all());

            request()->validate([
                'bank_name' => 'required | min:3 | max:10',
                'account_name' => 'required | min:3 | max:25',
                'account_number' => 'required | min:6 ',
                
            ]);

            Bankaccount::create([
                'user_id' => auth()->id(),
                'bank_name' => $request->bank_name,
                'account_name' => $request->account_name,
                'account_number' => $request->account_number,
            ]);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bankaccount  $bankaccount
     * @return \Illuminate\Http\Response
     */
    public function show(Bankaccount $bankaccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bankaccount  $bankaccount
     * @return \Illuminate\Http\Response
     */
    public function edit(Bankaccount $bankaccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bankaccount  $bankaccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bankaccount $bankaccount)
    {
        //
        $bankaccount->update(request([
            //'user_id' => auth()->id(),
            'bank_name',
            'account_name',
            'account_number',
        ]));
    return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bankaccount  $bankaccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bankaccount $bankaccount)
    {
        //
        $bankaccount->delete();
        return back();
    }
}
