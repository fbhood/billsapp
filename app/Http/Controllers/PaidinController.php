<?php

namespace App\Http\Controllers;

use App\Paidin;
use App\Imports\ImportPaidin;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class PaidinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request, Paidin $paidin)
    {
        //
        //dd($request->all());
        $file = $request->paidin;
        Excel::import(new ImportPaidin, $file);        
        return redirect('/')->with('success', 'All good!');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paidin  $paidin
     * @return \Illuminate\Http\Response
     */
    public function show(Paidin $paidin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paidin  $paidin
     * @return \Illuminate\Http\Response
     */
    public function edit(Paidin $paidin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paidin  $paidin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paidin $paidin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paidin  $paidin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paidin $paidin)
    {
        //
    }
}
