<?php

namespace App\Imports;

use App\Paidout;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPaidouts implements ToCollection, WithHeadingRow
{
   
    public function collection(Collection $rows)
    {
    

        foreach($rows as $row) 
        {
            try
            { 

                Paidout::create([
                    
                    'date'              => $row['date'],
                    'type'              => $row['type'],
                    'description'       => $row['description'],
                    'value'             => $row['value'],
                    'balance'           => $row['balance'],
                    
                
                ]);
            
            }
            catch (\Throwable $th) 
            {
                //throw $th;
                if($th){ 
                    continue;
                
                }
            } 
        
        }
    
    }

}
