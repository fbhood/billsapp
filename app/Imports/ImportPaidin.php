<?php

namespace App\Imports;

use App\Paidin;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportPaidin implements ToCollection, WithHeadingRow
{
    //use Importable;
    
    public function collection(Collection $rows)
    {
        foreach($rows as $row) 
        {
            
            try 
            {
                Paidin::create([
                    
                    'date'              => $row['date'],
                    'type'              => $row['type'],
                    'description'       => $row['description'],
                    'value'             => $row['value'],
                    'balance'           => $row['balance'],
                    /*'account_name'      => $row['account_name'],
                    'account_number'    => $row['account_number'],  */
                ]);
            } 
            catch (\Throwable $th) 
            {
                //throw $th;
                if($th){ 
                    continue;
                
                } 
            }
        
        }
    }
}
