<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    //
    
    /** 
     * @var array
     */
    protected $guarded = [];


    public function user(){
        return $this->belongsTo(App\User::class);
    }

    public function paidouts(){
       return $this->hasMany(App\Paidout::class);
    }

    public function paidin(){
       return $this->hasMany(App\Paidin::class);

    }

}
