### Installation: 

    Assuming that your system mets all the requirements for a Laravel Application v.5.7. 
  
        1. Make a folder in your projects directory and clone my application from the git repo
    
            ```
            mkdir billsApp
            cd billsApp

            ```
    
        2. Clone the Repository

            ```
                git clone https://fbhood@bitbucket.org/fbhood/billsapp.git

            ```
        
        3. Generate an app key

            From the main app folder generate an unique app key.
            
            ```
            php artisan key:generate
            ```


        4. Log into mysql, create a database for the app, 

        ```
            mysql -u mysql-username-here -p
            create database bills_app;
            exit;

        ``` 

        5. connect the created database in the .env file 
            Copy the example .env and edit the database connection configuration accordingly to your needs.

            ```
            cp .env.example .env
            nano .env

            ```
            Inside the .env file: 
            ```
                DB_CONNECTION=mysql
                DB_HOST=127.0.0.1
                DB_PORT=3306
                DB_DATABASE=bills_app
                DB_USERNAME=your-user-name-here
                DB_PASSWORD=your-password-here
            ```
            .env file example configuration


        6. Migrate the database

            ```
            cd billsApp
            php artisan migrate
            
            ```

    
    7. Depending on your environment, simply visit your http://localhost url from your browser to see the App welcome page.