<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $paidouts = App\Paidout::all();
    $paidins = App\Paidin::all();
    $auth_user_id = auth()->id();
    $bankaccount = App\Bankaccount::where('user_id', $auth_user_id)->get();
    $paidouts_results = DB::table('paidouts')->where('account_id', 1)->selectRaw("type, SUM(value) as paid")->groupBy('type')->pluck('paid','type');
    $paidins_results = DB::table('paidins')->where('account_id', 1)->selectRaw("type, SUM(value) as earned")->groupBy('type')->pluck('earned','type');
    
    $sum_paid_by_type = DB::table('paidouts')->where('account_id', 1)->selectRaw("type, SUM(value) as sum")->groupBy('type');
    $sum_earnings_by_type = DB::table('paidins')->where('account_id', 1)->selectRaw("type, SUM(value) as sum")->groupBy('type');
    
    $overview = $sum_earnings_by_type->union($sum_paid_by_type)->pluck('sum', 'type'); 
    $total_expenses = $paidouts->sum('value');
    $total_income   = $paidins->sum('value');
    $balance = $total_income + $total_expenses;
    //dd($balance);
    return view('welcome', compact('paidouts', 'bankaccount', 'paidins', 'paidouts_results','paidins_results','overview', 'balance', 'total_income','total_expenses'));
});

Route::get('/search', function(Request $request){
    // fetch all users from the users table where the profession is equal to the requested profession. 
    $search_request = request()->description;
      $paidins = DB::table('paidins')
      ->where([[strtolower('description'), 'LIKE', '%' . strtolower($search_request) . '%']]);
      $search_results = DB::table('paidouts')
      ->where([[strtolower('description'), 'LIKE', '%' . strtolower($search_request) . '%']])
      ->union($paidins)
      ->get();
  //dd($paidins);


    return view('search', compact('search_results'));

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/import-paidout', 'PaidoutController@import');
Route::post('/import-paidin', 'PaidinController@import');
Route::post('/bankaccounts', 'BankaccountController@store')->middleware('auth');
Route::put('/bankaccounts/{bankaccount}', 'BankaccountController@update')->middleware('auth');